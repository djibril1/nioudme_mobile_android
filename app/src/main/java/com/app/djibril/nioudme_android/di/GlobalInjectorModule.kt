package com.app.djibril.nioudme_android.di

import android.app.Activity
import android.content.Context
import com.app.djibril.nioudme_android.App
import dagger.Module
import dagger.Provides

@Module
class GlobalInjectorModule(var activity: Activity) {

    @Provides
    fun provideContext(): Context = activity

    @Provides
    fun provideApplication(): App = activity.application as App

}