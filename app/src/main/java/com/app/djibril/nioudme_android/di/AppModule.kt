package com.app.djibril.nioudme_android.di

import android.content.Context
import com.app.djibril.nioudme_android.App
import dagger.Module
import dagger.Provides

@Module
class AppModule(private val application: App) {

    @Provides
    fun provideApplicationContext(): Context = application


}