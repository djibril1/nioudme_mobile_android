package com.app.djibril.nioudme_android.di

import com.app.djibril.nioudme_android.App
import dagger.Component


@Component(modules = [(AppModule::class)])
interface AppComponent {
    fun inject(application: App)
}