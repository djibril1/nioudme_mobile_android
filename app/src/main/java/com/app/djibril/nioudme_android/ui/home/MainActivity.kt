package com.app.djibril.nioudme_android.ui.home

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.app.djibril.nioudme_android.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
